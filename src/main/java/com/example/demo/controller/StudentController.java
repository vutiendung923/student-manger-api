package com.example.demo.controller;

import com.example.demo.model.dto.StudentDto;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.UUID;

@RequestMapping("student")
@RestController
public class StudentController {
    private final StudentService studentService;


    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("")
    public ResponseEntity<ResponseEntity> getList(@NotNull @RequestParam("username") String username,
                                                  @NotNull @RequestParam("password") String password) throws IOException {
        return ResponseEntity.ok(new ResponseEntity(studentService.getList(username, password), HttpStatus.OK));
    }

    @GetMapping("detail")
    public ResponseEntity<ResponseEntity> getDetail(@RequestParam("id") Integer id,
                                                    @NotNull @RequestParam("username") String username,
                                                    @NotNull @RequestParam("password") String password) {
        return ResponseEntity.ok(new ResponseEntity(studentService.getDetail(username, password, id),HttpStatus.OK));
    }

    @PostMapping("add")
    public ResponseEntity addStudent(@NotNull @RequestParam("username") String username,
                                     @NotNull @RequestParam("password") String password,
                                     StudentDto studentDto) {
        studentService.addStudent(username, password, studentDto);
        return ResponseEntity.ok(new ResponseEntity(studentDto,HttpStatus.OK));
    }

    @PostMapping("update")
    public ResponseEntity updateStudent(@NotNull @RequestParam("username") String username,
                                        @NotNull @RequestParam("password") String password,
                                        StudentDto studentDto) {
        studentService.updateStudent(username, password, studentDto);
        return ResponseEntity.ok(new ResponseEntity(studentDto,HttpStatus.OK));
    }

    @GetMapping("delete")
    public ResponseEntity deleteStudent(@NotNull @RequestParam("username") String username,
                                        @NotNull @RequestParam("password") String password,
                                        @NotNull @RequestParam("id") Integer id) {
        studentService.deleteStudent(username, password,id);
        return ResponseEntity.ok(new ResponseEntity(HttpStatus.OK));
    }
}
