package com.example.demo.model.dto;

import com.example.demo.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StudentDto extends Student {
    private Integer id;

    private String username;

    private String password;

    private String name;

    private Integer age;

    private String code;

    private String className;

    private String address;

    private String mark;

    public StudentDto() {

    }

    public StudentDto(Student student) {
        this.id = student.getId();
        this.username = student.getUsername();
        this.password = student.getPassword();
        this.name = student.getName();
        this.age = student.getAge();
        this.code = student.getCode();
        this.className = student.getClassName();
        this.address = student.getAddress();
        this.mark = student.getMark();
    }
}
