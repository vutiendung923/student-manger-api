package com.example.demo.model.mapper;

import com.example.demo.entity.Student;
import com.example.demo.model.dto.StudentDto;


import java.util.UUID;

public class StudentMapper {
    public static StudentDto toStudentDto(Student student){
        StudentDto studentDto = new StudentDto();
        studentDto.setId(student.getId());
        studentDto.setUsername(student.getUsername());
        studentDto.setPassword(student.getPassword());
        studentDto.setCode(UUID.randomUUID().toString());
        studentDto.setName(student.getName());
        studentDto.setAddress(student.getAddress());
        studentDto.setAge(student.getAge());
        studentDto.setClassName(student.getClassName());
        studentDto.setMark(student.getMark());
        return studentDto;
    }
}
