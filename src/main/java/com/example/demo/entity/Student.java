package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private Integer id;

    private String username;

    private String password;

    private String name;

    private Integer age;

    private String code;

    private String className;

    private String address;

    private String mark;
}
