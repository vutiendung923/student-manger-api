package com.example.demo.service;

import com.example.demo.model.dto.StudentDto;

import java.io.IOException;
import java.util.List;

public interface StudentService {
    List<StudentDto> getList(String username, String password) throws IOException;
    StudentDto getDetail(String username, String password, Integer id);
    void addStudent(String username, String password, StudentDto studentDto);
    void updateStudent(String username, String password, StudentDto studentDto);
    void deleteStudent(String username, String password, Integer id);
}
