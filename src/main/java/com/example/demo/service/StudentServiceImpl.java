package com.example.demo.service;

import com.example.demo.entity.Student;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.dto.StudentDto;
import com.example.demo.model.mapper.StudentMapper;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Service
public class StudentServiceImpl implements StudentService{

    InputStream inputStream;

    Properties prop = new Properties();
    String propFileName = "config.properties";

    private static ArrayList<Student> students = new ArrayList<Student>();

    static {
        students.add(new Student(1,"thaianh","123@123a", "ThaiAnh", 21, ""
               ,"68B", "HN", "10"));
        students.add(new Student(2,"thaianh","123@123a", "ThaiAnh", 21, ""
                ,"68B", "HN", "10"));
        students.add(new Student(3,"thaianh","123@123a", "ThaiAnh", 21, ""
                ,"68B", "HN", "10"));
    }

    @Override
    public List<StudentDto> getList(String username, String password) throws IOException {
        ArrayList<StudentDto> list = new ArrayList<StudentDto>();
        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        for (Student student : students) {
            list.add(StudentMapper.toStudentDto(student));
        }
        if(username.toString().trim() == prop.getProperty("username") && password.toString().trim() == prop.getProperty("password")) {
            return list;
        }
        inputStream.close();
        return list;
    }

    @Override
    public StudentDto getDetail(String userName, String password, Integer id) {
        for (Student student : students) {
            if (student.getId() == id) {
                return StudentMapper.toStudentDto(student);
            }
        }



        throw new NotFoundException("Khong co hoc sinh nao");
    }

    @Override
    public void addStudent(String userName, String password, StudentDto studentDto) {
        Student student = new Student();
        students.add(StudentMapper.toStudentDto(student));
    }

    @Override
    public void updateStudent(String userName, String password,StudentDto studentDto) {
        Student student = new Student();
        students.add(StudentMapper.toStudentDto(student));
    }

    @Override
    public void deleteStudent(String userName, String password, Integer id) {
        Student student = new Student();
        students.remove(student.getId());
    }
}
